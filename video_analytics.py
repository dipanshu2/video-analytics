"""
For eye tracking, it is necessary to install these dependencies:

    !git clone https://github.com/antoinelame/GazeTracking.git
    !pip install -r /content/GazeTracking/requirements.txt
    !pip install cmake
    !pip install dlib==19.18.0

"""
#delete via os
import wave, moviepy,random,nltk, os
import moviepy.editor as mp
from nltk.featstruct import FeatureValueSet
import pandas as pd
import pydub
import cv2
import face_recognition
import speech_recognition as sr
import librosa
nltk.download('stopwords')
nltk.download('vader_lexicon')
nltk.download('wordnet')
nltk.download('averaged_perceptron_tagger')
from nltk.corpus import stopwords,wordnet
from collections import Counter
from pydub import AudioSegment
from nltk.sentiment.vader import SentimentIntensityAnalyzer
from PyDictionary import PyDictionary
from GazeTracking.gaze_tracking import GazeTracking
from pydub import AudioSegment


action_words = set(pd.read_csv('action_words.csv')['words'])
profanity_words = set(pd.read_csv('profanity.csv')['words'])
common_words = set(pd.read_csv('common_words.csv')['words'])
stop_words = set(stopwords.words('english'))


class Video_Analytics:
    """
    This class is meant to incorporate all functions related to video analytics.
    Path of the video file must be passed during creation of each new object.
    """

    def __init__(self,path_to_video_file,path_to_image):
        """
        Path of video file to be passed during creation of object.
        The video is first converted to .wav audio and then to text using
        Google API via recognizer class.
        Random naming is done to avoid name clash.

        :param path_to_video_file: str
        """
        self.image_path = path_to_image
        self.video_path = path_to_video_file

        clip = mp.VideoFileClip(path_to_video_file)
        self.audio_name = "audio" + str(random.randint(0,999999999999999)) + ".wav"
        clip.audio.write_audiofile(self.audio_name)


        recognizer = sr.Recognizer()
        audio = sr.AudioFile(self.audio_name)
        with audio as source:
            recognizer.adjust_for_ambient_noise(source, duration=0.15)
            audio_f = recognizer.record(source)

        self.text = recognizer.recognize_google(audio_f, language='en-IN')


    def grammar_correct(self):
        """
        This function prints:

        1. Number of errors
        2. Lists the errors
        3. Grammatically corrected sentence

        :param sample_text: str
        :return: None (only print)
        """
        from gingerit.gingerit import GingerIt
        result = GingerIt().parse(self.text)

        print(f'You made {len(result["corrections"])} mistakes\n')
        print("Errors:")
        for i in result['corrections']:
            del (i['start'])
            print(i)

        print(f'Corrected text: {result["result"]}')


    def filtered_text(self):
        """
        This function returns a list of filtered words from the text (in order),
        removing stopwords.

        :param text: str
        :return: list
        """

        return [w for w in self.text.split() if not w in stop_words]


    def pos_tags(self):
        """
        This function returns the PAST_OF_SPEECH tages for each word in the sentence.

        :return: nltk.pos_tag object
        """

        return nltk.pos_tag(self.text.split())


    def sentiment_score(self):
        """
        This function returns the Sentiment score of the transcribed text.
        Score is in the range [0-1]
        Higher the score, higher the positive sentiment of the sentence.
        :return: float
        """
        sid = SentimentIntensityAnalyzer()
        return sid.polarity_scores(self.text)['compound']



    def detect_pauses(self):
        """
        This function return True/False based on the number of pauses detected
        in the audio.
        Returns True if >=3 pauses detected.

        :return: bool
        """
        # from pydub.effects import normalize
        # normalized = normalize(AudioSegment.from_wav(path))
        # normalized.export(out_f='new.wav', format='wav')
        # p = "new.wav"
        non_silent = pydub.silence.detect_nonsilent(AudioSegment.from_wav(self.audio_name),
                                                    silence_thresh=-50,
                                                    min_silence_len=2000)
        return True if len(non_silent)>=3 else False


    def action_words_fraction(self):
        """
        This function returns the fraction of action words
        :return: float
        """
        action_words_count = 0
        for i in self.text.split():
            if i.lower() in action_words:
                action_words_count += 1
        return action_words_count / len(self.text.split())


    def profanity_check(self):
        """
        Returns bool based on profanity presence
        :return: bool
        """
        for i in self.text.split():
            if i.lower() in profanity_words:
                return True
        return False


    def filler_words_fraction(self):
        """
        Returns fraction of filler words
        :return: float
        """
        frac = 1 - (len(self.filtered_text()) / len(self.text.split()))
        return frac

    def most_frequent_words(self):
        """
        Returns a list of most frequently used words
        :return: list
        """
        dict1 = Counter(self.filtered_text())
        return sorted(dict1, key=lambda x: -dict1[x])[:6]


    def proper_noun_fractions(self):
        """
        Returns fraction of proper nouns used
        :return: float
        """
        proper_nouns = 0
        for i in self.pos_tags():
            if i[1] == 'NNP' or i[1] == 'NNPS':
                proper_nouns += 1

        return proper_nouns / len(self.text.split())


    def word_suggestions(self):
        """
        Returns possible replacements for overused words
        :return: dict
        """
        dictionary = PyDictionary()
        replacement_suggestions = dict.fromkeys(self.most_frequent_words())
        for i in replacement_suggestions:
            synonyms = dictionary.synonym(i)
            try:
                replacement_suggestions[i] = list(synonyms)[:5]
            except:
                replacement_suggestions[i] = synonyms
        return replacement_suggestions


    def unique_words_fraction(self):
        """
        Returns a tuple of size 2.
        first element is a set of unique words used.
        second element is the fraction of unique words used
        :return: tuple
        """
        unique = [i for i in self.text.split() if i.replace(".", "").lower() not in common_words]
        to_be_removed = []
        for i in unique:
            try:
                a = int(i)
                to_be_removed.append(i)
            except:
                pass
        for i in to_be_removed:
            unique.remove(i)
        return (set(unique), len(unique)/len(self.text.split()))

    def speaking_speed(self):
        """
        Returns words spoken per minute
        :return: float
        """
        samples, sampling_rate = librosa.load(self.audio_name)
        speed = (len(self.text.split()) / (len(samples) / sampling_rate)) * 60
        return speed


    def facial_analysis(self):
        """
        Performs the facial analysis.
        Detects,matches and checks eye position
        :return: 3 sized tuple (face detected, face matched, correct eye position)
        """
        cap = cv2.VideoCapture(self.video_path)
        test = cv2.imread(self.image_path)
        test = cv2.cvtColor(test, cv2.COLOR_BGR2RGB)
        enc2 = face_recognition.face_encodings(test)

        face_detected = []
        face_matched = []
        eye_pos = []
        count = 0

        while (cap.isOpened()):
            try:
                ret, frame = cap.read()
                if count % 30 == 0:
                    #

                    gaze.refresh(frame)
                    if gaze.is_right():
                        eye_pos.append("R")
                    elif gaze.is_left():
                        eye_pos.append("L")
                    elif gaze.is_center():
                        eye_pos.append("C")

                    #
                    rgb = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
                    test_image = face_recognition.face_encodings(rgb)
                    faces = face_recognition.face_locations(rgb)
                    if len(faces) > 0:
                        face_detected.append(True)
                        if True in face_recognition.compare_faces(test_image, enc2[0]):
                            face_matched.append(True)
                        else:
                            face_matched.append(False)
                    else:
                        face_detected.append(False)

                count += 1
            except:
                break
        try:        
            if face_detected.count(True) / len(face_detected) >= 0.9:
                detected = True
            else:
                detected = False
        except:
            detected = False        

        try:
            if face_matched.count(True)/len(face_matched) >= 0.9:
                matched = True
            else:
                matched = False
        except:
            matched = False

         
        if len(eye_pos) == 0:
            eye = "Too far"
        else:
            if eye_pos.count('C')/len(eye_pos) >=0.75:
                eye = True
            else:
                eye = False

        return (detected,matched,eye)


    def complete_analysis(self):
        print(self.text)
        print("--------------------------------------------------------")
        print("ANALYSIS:\n")
        self.grammar_correct()
        print(f"Sentiment score: {self.sentiment_score()}")
        print(f"Pauses taken: {self.detect_pauses()}")
        print(f"Action words fraction: {self.action_words_fraction()}")
        print(f"Profanity present: {self.profanity_check()}")
        print(f"Filler words fraction: {self.filler_words_fraction()}")
        print(f"Most used words: {self.most_frequent_words()}")
        print(f"Proper noun fraction: {self.proper_noun_fractions()}")
        print(f"Word suggestions: {self.word_suggestions()}")
        print(f"Unique words analysis: {self.unique_words_fraction()}")
        print(f"Speaking speed: {self.speaking_speed()}")
        print(f"Facial Analysis (face detected,face matched, centered gaze): {self.facial_analysis()}")
        os.remove(self.audio_name)

obj = Video_Analytics('short_video.mp4','whitebg.jpg')
obj.complete_analysis()